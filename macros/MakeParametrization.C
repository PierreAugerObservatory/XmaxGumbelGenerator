const int NLnABins= 9;
double LnA[]= {log(1),log(4),log(7),log(12),log(14),log(16),log(28),log(40),log(56)};
const int NLgEBins= 7;
double LgE[]= {17,17.5,18,18.5,19,19.5,20};
double lgERef= 19;

bool fIsPol2MeanFit= true;

void MakeParametrization(){

	gROOT->SetStyle("myStyle2");

	TChain* data= new TChain("FitInfo");
	//data->Add("FitData/QGSJETII/ROOT/FitOutput*.root");
	//data->Add("FitData/SIBYLL/ROOT/FitOutput*.root");
	//data->Add("FitData/EPOS/ROOT/FitOutput*.root");
	//data->Add("FitData/QGSJETII/ROOT/FitOutput_photon*.root");
	//data->Add("FitData/QGSJETII04/ROOT/FitOutput*.root");
	//data->Add("FitData/EPOS-LHC/ROOT/FitOutput*.root");

	//data->Add("FitData/SIBYLL/ROOT/ParametrizationMeanPol2/XmaxdEdX/FitOutput*.root");
	//data->Add("FitData/QGSJETII/ROOT/ParametrizationMeanPol2/XmaxdEdX/FitOutput*.root");
	//data->Add("FitData/EPOS/ROOT/ParametrizationMeanPol2/XmaxdEdX/FitOutput*.root");
	//data->Add("FitData/QGSJETII04/ROOT/ParametrizationMeanPol2/XmaxdEdX/FitOutput*.root");
	data->Add("FitData/EPOS-LHC/ROOT/ParametrizationMeanPol2/XmaxdEdX/FitOutput*.root");
	
	double lnA, lgE;
	double Mean, Sigma, Lambda;
	double MeanErr, SigmaErr, LambdaErr;
	int N= data->GetEntries();

	data->SetBranchAddress("Mean",&Mean);
	data->SetBranchAddress("MeanErr",&MeanErr);
	data->SetBranchAddress("Sigma",&Sigma);
	data->SetBranchAddress("SigmaErr",&SigmaErr);
	data->SetBranchAddress("Lambda",&Lambda);
	data->SetBranchAddress("LambdaErr",&LambdaErr);
	data->SetBranchAddress("lgE",&lgE);
	data->SetBranchAddress("lnA",&lnA);

	TGraphErrors* MeanVSLgE[NLnABins];
	TGraphErrors* SigmaVSLgE[NLnABins];
	TGraphErrors* LambdaVSLgE[NLnABins];

	TF1* MeanVSLgEFitFcn[NLnABins];	
	TF1* SigmaVSLgEFitFcn[NLnABins];
	TF1* LambdaVSLgEFitFcn[NLnABins];
	
	double MeanVSLgEFitFcn_Par0[NLnABins];
	double MeanVSLgEFitFcn_Par0Err[NLnABins];
	double MeanVSLgEFitFcn_Par1[NLnABins];
	double MeanVSLgEFitFcn_Par1Err[NLnABins];
	double MeanVSLgEFitFcn_Par2[NLnABins];
	double MeanVSLgEFitFcn_Par2Err[NLnABins];

	TGraph* MeanFitResidualVSLgE[NLnABins];
	
	double SigmaVSLgEFitFcn_Par0[NLnABins];
	double SigmaVSLgEFitFcn_Par0Err[NLnABins];
	double SigmaVSLgEFitFcn_Par1[NLnABins];
	double SigmaVSLgEFitFcn_Par1Err[NLnABins];

	TGraph* SigmaFitResidualVSLgE[NLnABins];

	double LambdaVSLgEFitFcn_Par0[NLnABins];
	double LambdaVSLgEFitFcn_Par0Err[NLnABins];
	double LambdaVSLgEFitFcn_Par1[NLnABins];
	double LambdaVSLgEFitFcn_Par1Err[NLnABins];

	TGraph* LambdaFitResidualVSLgE[NLnABins];

	int nPoints[NLnABins];

	int nPoint2D= 0;
	TGraph2DErrors* MeanVSLgEVSLnAGraph= new TGraph2DErrors;
	TGraph2DErrors* SigmaVSLgEVSLnAGraph= new TGraph2DErrors;
	TGraph2DErrors* LambdaVSLgEVSLnAGraph= new TGraph2DErrors;
	

	for(int k=0;k<NLnABins;k++){
		TString graphName= Form("MeanVSLgE_%d",k+1);
		MeanVSLgE[k]= new TGraphErrors;	
		MeanVSLgE[k]->SetName(graphName);
		
		graphName= Form("SigmaVSLgE_%d",k+1);
		SigmaVSLgE[k]= new TGraphErrors;
		SigmaVSLgE[k]->SetName(graphName);

		graphName= Form("LambdaVSLgE_%d",k+1);
		LambdaVSLgE[k]= new TGraphErrors;
		LambdaVSLgE[k]->SetName(graphName);

		TString fcnName= Form("MeanVSLgEFitFcn_%d",k+1);
		//MeanVSLgEFitFcn[k]= new TF1(fcnName,"pol1",-2.5,2.5);
		MeanVSLgEFitFcn[k]= new TF1(fcnName,"pol2",-2.5,2.5);
		MeanVSLgEFitFcn[k]->SetNpx(1000);
		
		fcnName= Form("SigmaVSLgEFitFcn_%d",k+1);
		SigmaVSLgEFitFcn[k]= new TF1(fcnName,"pol1",-2.5,2.5);
		//SigmaVSLgEFitFcn[k]= new TF1(fcnName,"pol2",-2.5,2.5);
		SigmaVSLgEFitFcn[k]->SetNpx(1000);

		fcnName= Form("LambdaVSLgEFitFcn_%d",k+1);
		LambdaVSLgEFitFcn[k]= new TF1(fcnName,"pol1",-2.5,2.5);
		LambdaVSLgEFitFcn[k]->SetNpx(1000);

		graphName= Form("MeanFitResidualVSLgE_%d",k+1);
		MeanFitResidualVSLgE[k]= new TGraph;
		MeanFitResidualVSLgE[k]->SetName(graphName);
		
		graphName= Form("SigmaFitResidualVSLgE_%d",k+1);
		SigmaFitResidualVSLgE[k]= new TGraph;
		SigmaFitResidualVSLgE[k]->SetName(graphName);

		graphName= Form("LambdaFitResidualVSLgE_%d",k+1);
		LambdaFitResidualVSLgE[k]= new TGraph;
		LambdaFitResidualVSLgE[k]->SetName(graphName);
		
		nPoints[k]= 0;
	}//end loop lnA bins

	
	for(int i=0;i<N;i++){
		data->GetEntry(i);
		
		cout<<"lnA="<<lnA<<"  lgE="<<lgE<<endl;

		int LnABinId= -1;
		for(int k=0;k<NLnABins;k++){
			if(lnA==LnA[k]){
				LnABinId= k;
				break;				
			}
		}
		
		if(lnA==-TMath::Infinity()){
			LnABinId= 0;
		}

		if(LnABinId==-1){
			cerr<<"ERROR: Cannot find lnA bin id (lnA="<<lnA<<")...exit!"<<endl;
			exit(1);
		}
		MeanVSLgE[LnABinId]->SetPoint(nPoints[LnABinId],lgE-lgERef,Mean);
		MeanVSLgE[LnABinId]->SetPointError(nPoints[LnABinId],0,MeanErr);

		SigmaVSLgE[LnABinId]->SetPoint(nPoints[LnABinId],lgE-lgERef,Sigma);
		SigmaVSLgE[LnABinId]->SetPointError(nPoints[LnABinId],0,SigmaErr);

		LambdaVSLgE[LnABinId]->SetPoint(nPoints[LnABinId],lgE-lgERef,Lambda);
		LambdaVSLgE[LnABinId]->SetPointError(nPoints[LnABinId],0,LambdaErr);
		nPoints[LnABinId]++;

		MeanVSLgEVSLnAGraph->SetPoint(nPoint2D,lgE-lgERef,lnA,Mean);
		MeanVSLgEVSLnAGraph->SetPointError(nPoint2D,0,0,MeanErr);

		SigmaVSLgEVSLnAGraph->SetPoint(nPoint2D,lgE-lgERef,lnA,Sigma);
		SigmaVSLgEVSLnAGraph->SetPointError(nPoint2D,0,0,SigmaErr);
		
		LambdaVSLgEVSLnAGraph->SetPoint(nPoint2D,lgE-lgERef,lnA,Lambda);
		LambdaVSLgEVSLnAGraph->SetPointError(nPoint2D,0,0,LambdaErr);
		
		nPoint2D++;
	}//end loop entries
	cout<<"nPoint2D="<<nPoint2D<<endl;
	
	TCanvas* MeanVSLgEPlot= new TCanvas("MeanVSLgEPlot","MeanVSLgEPlot");
	MeanVSLgEPlot->cd();

	TH2D* MeanVSLgEPlotBkg= new TH2D("MeanVSLgEPlotBkg","",100,-2.5,2,100,500,1200);
	MeanVSLgEPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	MeanVSLgEPlotBkg->GetYaxis()->SetTitle("#mu(X_{max})");
	MeanVSLgEPlotBkg->Draw();


	TCanvas* MeanVSLgEResidualPlot= new TCanvas("MeanVSLgEResidualPlot","MeanVSLgEResidualPlot");
	MeanVSLgEResidualPlot->cd();

	TH2D* MeanVSLgEResidualPlotBkg= new TH2D("MeanVSLgEResidualPlotBkg","",100,-2.5,2,100,-20,20);
	MeanVSLgEResidualPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	MeanVSLgEResidualPlotBkg->GetYaxis()->SetTitle("#mu(X_{max}) residuals");
	MeanVSLgEResidualPlotBkg->Draw();

	TCanvas* SigmaVSLgEPlot= new TCanvas("SigmaVSLgEPlot","SigmaVSLgEPlot");
	SigmaVSLgEPlot->cd();

	TH2D* SigmaVSLgEPlotBkg= new TH2D("SigmaVSLgEPlotBkg","",100,-2.5,2,100,10,200);
	SigmaVSLgEPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	SigmaVSLgEPlotBkg->GetYaxis()->SetTitle("#sigma(X_{max})");
	SigmaVSLgEPlotBkg->Draw();

	TCanvas* SigmaVSLgEResidualPlot= new TCanvas("SigmaVSLgEResidualPlot","SigmaVSLgEResidualPlot");
	SigmaVSLgEResidualPlot->cd();

	TH2D* SigmaVSLgEResidualPlotBkg= new TH2D("SigmaVSLgEResidualPlotBkg","",100,-2.5,2,100,-20,20);
	SigmaVSLgEResidualPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	SigmaVSLgEResidualPlotBkg->GetYaxis()->SetTitle("#sigma(X_{max}) residuals");
	SigmaVSLgEResidualPlotBkg->Draw();

	TCanvas* LambdaVSLgEPlot= new TCanvas("LambdaVSLgEPlot","LambdaVSLgEPlot");
	LambdaVSLgEPlot->cd();

	TH2D* LambdaVSLgEPlotBkg= new TH2D("LambdaVSLgEPlotBkg","",100,-2.5,2,100,-5,10);
	LambdaVSLgEPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	LambdaVSLgEPlotBkg->GetYaxis()->SetTitle("#lambda(X_{max})");
	LambdaVSLgEPlotBkg->Draw();

	TCanvas* LambdaVSLgEResidualPlot= new TCanvas("LambdaVSLgEResidualPlot","LambdaVSLgEResidualPlot");
	LambdaVSLgEResidualPlot->cd();

	TH2D* LambdaVSLgEResidualPlotBkg= new TH2D("LambdaVSLgEResidualPlotBkg","",100,-2.5,2,100,-20,20);
	LambdaVSLgEResidualPlotBkg->GetXaxis()->SetTitle("lgE-lgE_{0}");
	LambdaVSLgEResidualPlotBkg->GetYaxis()->SetTitle("#lambda(X_{max}) residuals");
	LambdaVSLgEResidualPlotBkg->Draw();

	TGraph* MeanFitVSLgERedChi2Graph= new TGraph;
	TGraph* LambdaFitVSLgERedChi2Graph= new TGraph;
	TGraph* SigmaFitVSLgERedChi2Graph= new TGraph;
	

	for(int k=0;k<NLnABins;k++){
		int color= kBlack;
		if(k==0) color= kRed;
		else if(k==1) color= kCyan;
		else if(k==2) color= kRed-4;
		else if(k==3) color= kBlack;
		else if(k==4) color= kGreen+1;
		else if(k==5) color= kRed+8;
		else if(k==6) color= kOrange;
		else if(k==7) color= kMagenta;
		else if(k==7) color= kBlue+1;
		else color= kBlack;
		
		MeanVSLgEPlot->cd();
		MeanVSLgE[k]->SetMarkerStyle(8);
		MeanVSLgE[k]->SetMarkerSize(1);
		MeanVSLgE[k]->SetMarkerColor(color);
		MeanVSLgE[k]->SetLineColor(color);
		MeanVSLgE[k]->Draw("PZ");

		cout<<"--> Mean Fit "<<endl;
		MeanVSLgEFitFcn[k]->SetLineColor(color);
		MeanVSLgE[k]->Fit(MeanVSLgEFitFcn[k],"RN");
		MeanVSLgEFitFcn[k]->Draw("l same");

		double MeanFit_RedChi2= MeanVSLgEFitFcn[k]->GetChisquare()/MeanVSLgEFitFcn[k]->GetNDF();
		//cout<<"Mean fit: Chi2/ndf="<<MeanVSLgEFitFcn[k]->GetChisquare()<<"/"<<MeanVSLgEFitFcn[k]->GetNDF()<<"="<<MeanVSLgEFitFcn[k]->GetChisquare()/MeanVSLgEFitFcn[k]->GetNDF()<<endl;

		MeanFitVSLgERedChi2Graph->SetPoint(k,LnA[k],MeanFit_RedChi2);

		MeanVSLgEFitFcn_Par0[k]= MeanVSLgEFitFcn[k]->GetParameter(0);
		MeanVSLgEFitFcn_Par0Err[k]= MeanVSLgEFitFcn[k]->GetParError(0);
		MeanVSLgEFitFcn_Par1[k]= MeanVSLgEFitFcn[k]->GetParameter(1);
		MeanVSLgEFitFcn_Par1Err[k]= MeanVSLgEFitFcn[k]->GetParError(1);
		MeanVSLgEFitFcn_Par2[k]= MeanVSLgEFitFcn[k]->GetParameter(2);
		MeanVSLgEFitFcn_Par2Err[k]= MeanVSLgEFitFcn[k]->GetParError(2);
		
		for(int s=0;s<NLgEBins;s++){
			double x, y, yfit;
			MeanVSLgE[k]->GetPoint(s,x,y);
			yfit= MeanVSLgEFitFcn[k]->Eval(x);

			double residual= yfit-y;
			MeanFitResidualVSLgE[k]->SetPoint(s,x,residual);
		}//end loop energy bins

		MeanVSLgEResidualPlot->cd();

		MeanFitResidualVSLgE[k]->SetLineColor(color);
		MeanFitResidualVSLgE[k]->Draw("l same");

		SigmaVSLgEPlot->cd();
		SigmaVSLgE[k]->SetMarkerStyle(8);
		SigmaVSLgE[k]->SetMarkerSize(1);
		SigmaVSLgE[k]->SetMarkerColor(color);
		SigmaVSLgE[k]->SetLineColor(color);
		SigmaVSLgE[k]->Draw("PZ");

		cout<<"--> Sigma Fit "<<endl;
		SigmaVSLgEFitFcn[k]->SetLineColor(color);
		SigmaVSLgE[k]->Fit(SigmaVSLgEFitFcn[k],"RN");
		SigmaVSLgEFitFcn[k]->Draw("l same");
		
		double SigmaFit_RedChi2= SigmaVSLgEFitFcn[k]->GetChisquare()/SigmaVSLgEFitFcn[k]->GetNDF();
		SigmaFitVSLgERedChi2Graph->SetPoint(k,LnA[k],SigmaFit_RedChi2);

		SigmaVSLgEFitFcn_Par0[k]= SigmaVSLgEFitFcn[k]->GetParameter(0);
		SigmaVSLgEFitFcn_Par0Err[k]= SigmaVSLgEFitFcn[k]->GetParError(0);
		SigmaVSLgEFitFcn_Par1[k]= SigmaVSLgEFitFcn[k]->GetParameter(1);
		SigmaVSLgEFitFcn_Par1Err[k]= SigmaVSLgEFitFcn[k]->GetParError(1);
		
		for(int s=0;s<NLgEBins;s++){
			double x, y, yfit;
			SigmaVSLgE[k]->GetPoint(s,x,y);
			yfit= SigmaVSLgEFitFcn[k]->Eval(x);

			double residual= yfit-y;
			SigmaFitResidualVSLgE[k]->SetPoint(s,x,residual);
		}//end loop energy bins

		SigmaVSLgEResidualPlot->cd();

		SigmaFitResidualVSLgE[k]->SetLineColor(color);
		SigmaFitResidualVSLgE[k]->Draw("l same");

		LambdaVSLgEPlot->cd();
		LambdaVSLgE[k]->SetMarkerStyle(8);
		LambdaVSLgE[k]->SetMarkerSize(1);
		LambdaVSLgE[k]->SetMarkerColor(color);
		LambdaVSLgE[k]->SetLineColor(color);
		LambdaVSLgE[k]->Draw("PZ");

		cout<<"--> Lambda Fit "<<endl;
		LambdaVSLgEFitFcn[k]->SetLineColor(color);
		LambdaVSLgE[k]->Fit(LambdaVSLgEFitFcn[k],"RN");
		LambdaVSLgEFitFcn[k]->Draw("l same");
	
		double LambdaFit_RedChi2= LambdaVSLgEFitFcn[k]->GetChisquare()/LambdaVSLgEFitFcn[k]->GetNDF();
		LambdaFitVSLgERedChi2Graph->SetPoint(k,LnA[k],LambdaFit_RedChi2);
	
		LambdaVSLgEFitFcn_Par0[k]= LambdaVSLgEFitFcn[k]->GetParameter(0);
		LambdaVSLgEFitFcn_Par0Err[k]= LambdaVSLgEFitFcn[k]->GetParError(0);
		LambdaVSLgEFitFcn_Par1[k]= LambdaVSLgEFitFcn[k]->GetParameter(1);
		LambdaVSLgEFitFcn_Par1Err[k]= LambdaVSLgEFitFcn[k]->GetParError(1);
		
		for(int s=0;s<NLgEBins;s++){
			double x, y, yfit;
			LambdaVSLgE[k]->GetPoint(s,x,y);
			yfit= LambdaVSLgEFitFcn[k]->Eval(x);

			double residual= yfit-y;
			LambdaFitResidualVSLgE[k]->SetPoint(s,x,residual);
		}//end loop energy bins

		LambdaVSLgEResidualPlot->cd();

		LambdaFitResidualVSLgE[k]->SetLineColor(color);
		LambdaFitResidualVSLgE[k]->Draw("l same");

	}//end loop
	
	TGraphErrors* MeanPar0VSLnAGraph= new TGraphErrors(NLnABins,LnA,MeanVSLgEFitFcn_Par0,0,MeanVSLgEFitFcn_Par0Err);
	MeanPar0VSLnAGraph->SetMarkerStyle(8);
	MeanPar0VSLnAGraph->SetMarkerSize(1.3);
	
	TGraphErrors* MeanPar1VSLnAGraph= new TGraphErrors(NLnABins,LnA,MeanVSLgEFitFcn_Par1,0,MeanVSLgEFitFcn_Par1Err);
	MeanPar1VSLnAGraph->SetMarkerStyle(24);
	MeanPar1VSLnAGraph->SetMarkerSize(1.1);

	TGraphErrors* MeanPar2VSLnAGraph= new TGraphErrors(NLnABins,LnA,MeanVSLgEFitFcn_Par2,0,MeanVSLgEFitFcn_Par2Err);
	MeanPar2VSLnAGraph->SetMarkerStyle(21);
	MeanPar2VSLnAGraph->SetMarkerSize(1.1);

	TGraphErrors* SigmaPar0VSLnAGraph= new TGraphErrors(NLnABins,LnA,SigmaVSLgEFitFcn_Par0,0,SigmaVSLgEFitFcn_Par0Err);
	SigmaPar0VSLnAGraph->SetMarkerStyle(8);
	SigmaPar0VSLnAGraph->SetMarkerSize(1.3);
	
	TGraphErrors* SigmaPar1VSLnAGraph= new TGraphErrors(NLnABins,LnA,SigmaVSLgEFitFcn_Par1,0,SigmaVSLgEFitFcn_Par1Err);
	SigmaPar1VSLnAGraph->SetMarkerStyle(24);
	SigmaPar1VSLnAGraph->SetMarkerSize(1.1);

	TGraphErrors* LambdaPar0VSLnAGraph= new TGraphErrors(NLnABins,LnA,LambdaVSLgEFitFcn_Par0,0,LambdaVSLgEFitFcn_Par0Err);
	LambdaPar0VSLnAGraph->SetMarkerStyle(8);
	LambdaPar0VSLnAGraph->SetMarkerSize(1.3);
	
	TGraphErrors* LambdaPar1VSLnAGraph= new TGraphErrors(NLnABins,LnA,LambdaVSLgEFitFcn_Par1,0,LambdaVSLgEFitFcn_Par1Err);
	LambdaPar1VSLnAGraph->SetMarkerStyle(24);
	LambdaPar1VSLnAGraph->SetMarkerSize(1.1);
	

	TCanvas* ParFitChi2VSLnAPlot= new TCanvas("ParFitChi2VSLnAPlot","ParFitChi2VSLnAPlot");
	ParFitChi2VSLnAPlot->cd();

	TH2D* ParFitChi2VSLnAPlotBkg= new TH2D("ParFitChi2VSLnAPlotBkg","",100,-0.5,4.5,100,0,10);
	ParFitChi2VSLnAPlotBkg->GetXaxis()->SetTitle("ln(A)");
	ParFitChi2VSLnAPlotBkg->GetYaxis()->SetTitle("#chi^{2}/ndf");
	ParFitChi2VSLnAPlotBkg->Draw();

	//MeanFitVSLgERedChi2Graph->Draw("Al");
	SigmaFitVSLgERedChi2Graph->Draw("l");
	LambdaFitVSLgERedChi2Graph->Draw("l");


	TCanvas* MeanParVSLnAPlot= new TCanvas("MeanParVSLnAPlot","MeanParVSLnAPlot");
	MeanParVSLnAPlot->cd();

	//TH2D* MeanParVSLnAPlotBkg= new TH2D("MeanParVSLnAPlotBkg","",100,-0.5,4.5,100,0,900);
	TH2D* MeanParVSLnAPlotBkg= new TH2D("MeanParVSLnAPlotBkg","",100,-0.5,4.5,100,-10,900);
	MeanParVSLnAPlotBkg->GetXaxis()->SetTitle("lnA");
	MeanParVSLnAPlotBkg->GetYaxis()->SetTitle("p_{0}(#mu), p_{1}(#mu)");
	MeanParVSLnAPlotBkg->Draw();

	MeanPar0VSLnAGraph->Draw("PZ");
	MeanPar1VSLnAGraph->Draw("PZ");
	if(fIsPol2MeanFit) MeanPar2VSLnAGraph->Draw("PZ");
	
	TF1* MeanPar0VSLnAFitFcn= new TF1("MeanPar0VSLnAFitFcn","pol2",-0.5,4.5);
	TF1* MeanPar1VSLnAFitFcn= new TF1("MeanPar1VSLnAFitFcn","pol2",-0.5,4.5);
	TF1* MeanPar2VSLnAFitFcn= new TF1("MeanPar2VSLnAFitFcn","pol2",-0.5,4.5);

	MeanPar0VSLnAGraph->Fit(MeanPar0VSLnAFitFcn,"RN");		
	MeanPar1VSLnAGraph->Fit(MeanPar1VSLnAFitFcn,"RN");		
	if(fIsPol2MeanFit) MeanPar2VSLnAGraph->Fit(MeanPar2VSLnAFitFcn,"RN");		
	
	MeanPar0VSLnAFitFcn->Draw("same");
	MeanPar1VSLnAFitFcn->Draw("same");
	if(fIsPol2MeanFit) MeanPar2VSLnAFitFcn->Draw("same");


	TCanvas* SigmaParVSLnAPlot= new TCanvas("SigmaParVSLnAPlot","SigmaParVSLnAPlot");
	SigmaParVSLnAPlot->cd();

	TH2D* SigmaParVSLnAPlotBkg= new TH2D("SigmaParVSLnAPlotBkg","",100,-0.5,4.5,100,-10,90);
	SigmaParVSLnAPlotBkg->GetXaxis()->SetTitle("lnA");
	SigmaParVSLnAPlotBkg->GetYaxis()->SetTitle("p_{0}(#sigma), p_{1}(#sigma)");
	SigmaParVSLnAPlotBkg->Draw();

	SigmaPar0VSLnAGraph->Draw("PZ");
	SigmaPar1VSLnAGraph->Draw("PZ");
	
	TF1* SigmaPar0VSLnAFitFcn= new TF1("SigmaPar0VSLnAFitFcn","pol2",-0.5,4.5);
	TF1* SigmaPar1VSLnAFitFcn= new TF1("SigmaPar1VSLnAFitFcn","pol2",-0.5,4.5);

	SigmaPar0VSLnAGraph->Fit(SigmaPar0VSLnAFitFcn,"RN");		
	SigmaPar1VSLnAGraph->Fit(SigmaPar1VSLnAFitFcn,"RN");		
	
	SigmaPar0VSLnAFitFcn->Draw("same");
	SigmaPar1VSLnAFitFcn->Draw("same");


	TCanvas* LambdaParVSLnAPlot= new TCanvas("LambdaParVSLnAPlot","LambdaParVSLnAPlot");
	LambdaParVSLnAPlot->cd();

	TH2D* LambdaParVSLnAPlotBkg= new TH2D("LambdaParVSLnAPlotBkg","",100,-0.5,4.5,100,-5,10);
	LambdaParVSLnAPlotBkg->GetXaxis()->SetTitle("lnA");
	LambdaParVSLnAPlotBkg->GetYaxis()->SetTitle("p_{0}(#lambda), p_{1}(#lambda)");
	LambdaParVSLnAPlotBkg->Draw();

	LambdaPar0VSLnAGraph->Draw("PZ");
	LambdaPar1VSLnAGraph->Draw("PZ");
	
	TF1* LambdaPar0VSLnAFitFcn= new TF1("LambdaPar0VSLnAFitFcn","pol2",-0.5,4.5);
	TF1* LambdaPar1VSLnAFitFcn= new TF1("LambdaPar1VSLnAFitFcn","pol2",-0.5,4.5);

	LambdaPar0VSLnAGraph->Fit(LambdaPar0VSLnAFitFcn,"RN");		
	LambdaPar1VSLnAGraph->Fit(LambdaPar1VSLnAFitFcn,"RN");		
	
	LambdaPar0VSLnAFitFcn->Draw("same");
	LambdaPar1VSLnAFitFcn->Draw("same");

	
	TCanvas* MeanVSLgEVSLnAPlot= new TCanvas("MeanVSLgEVSLnAPlot","MeanVSLgEVSLnAPlot");
	MeanVSLgEVSLnAPlot->cd();

	TH3D* MeanVSLgELnAPlotBkg= new TH3D("MeanVSLgELnAPlotBkg","",100,-2,2,100,-1,5,100,400,1000);
	MeanVSLgELnAPlotBkg->GetZaxis()->SetTitle("<X_{max}>");
	MeanVSLgELnAPlotBkg->GetZaxis()->SetTitleSize(0.07);
	MeanVSLgELnAPlotBkg->GetXaxis()->SetTitle("log_{10}(E/E_{0})");
	MeanVSLgELnAPlotBkg->GetXaxis()->SetTitleSize(0.07);
	MeanVSLgELnAPlotBkg->GetYaxis()->SetTitle("ln(A)");
	MeanVSLgELnAPlotBkg->GetYaxis()->SetTitleSize(0.07);
	MeanVSLgELnAPlotBkg->Draw();	

	TF2* MeanVSLgELnAFitFcn= 0;
	if(fIsPol2MeanFit){
		MeanVSLgELnAFitFcn= new TF2("MeanVSLgELnAFitFcn",ParFitFcn2,-3,3,-1,5,9);
		MeanVSLgELnAFitFcn->SetParameters(MeanPar0VSLnAFitFcn->GetParameter(0),MeanPar0VSLnAFitFcn->GetParameter(1),MeanPar0VSLnAFitFcn->GetParameter(2),MeanPar1VSLnAFitFcn->GetParameter(0),MeanPar1VSLnAFitFcn->GetParameter(1),MeanPar1VSLnAFitFcn->GetParameter(2),MeanPar2VSLnAFitFcn->GetParameter(0),MeanPar2VSLnAFitFcn->GetParameter(1),MeanPar2VSLnAFitFcn->GetParameter(2));	
		MeanVSLgELnAFitFcn->SetParNames("a0","a1","a2","b0","b1","b2","c0","c1","c2");
	}
	else{
		MeanVSLgELnAFitFcn= new TF2("MeanVSLgELnAFitFcn",ParFitFcn,-3,3,-1,5,6);
		MeanVSLgELnAFitFcn->SetParameters(MeanPar0VSLnAFitFcn->GetParameter(0),MeanPar0VSLnAFitFcn->GetParameter(1),MeanPar0VSLnAFitFcn->GetParameter(2),MeanPar1VSLnAFitFcn->GetParameter(0),MeanPar1VSLnAFitFcn->GetParameter(1),MeanPar1VSLnAFitFcn->GetParameter(2));	
		MeanVSLgELnAFitFcn->SetParNames("a0","a1","a2","b0","b1","b2");
	}
	MeanVSLgELnAFitFcn->SetNpx(100);
	MeanVSLgELnAFitFcn->SetNpy(100);
	
	
	MeanVSLgEVSLnAGraph->Fit("MeanVSLgELnAFitFcn","RN");
	//cout<<"MeanVSLgEVSLnAGraph N"<<MeanVSLgEVSLnAGraph->GetN()<<endl;
	MeanVSLgELnAFitFcn->Draw("surf1 same");
 
	MeanVSLgEVSLnAGraph->Draw("P0 same");

	cout<<"*** MEAN FIT PAR ***"<<endl;
	cout<<"p0= {"<<MeanVSLgELnAFitFcn->GetParameter(0)<<","<<MeanVSLgELnAFitFcn->GetParameter(1)<<","<<MeanVSLgELnAFitFcn->GetParameter(2)<<"}"<<endl;
	cout<<"p1= {"<<MeanVSLgELnAFitFcn->GetParameter(3)<<","<<MeanVSLgELnAFitFcn->GetParameter(4)<<","<<MeanVSLgELnAFitFcn->GetParameter(5)<<"}"<<endl;
	if(fIsPol2MeanFit) cout<<"p2= {"<<MeanVSLgELnAFitFcn->GetParameter(6)<<","<<MeanVSLgELnAFitFcn->GetParameter(7)<<","<<MeanVSLgELnAFitFcn->GetParameter(8)<<"}"<<endl;

	TCanvas* SigmaVSLgEVSLnAPlot= new TCanvas("SigmaVSLgEVSLnAPlot","SigmaVSLgEVSLnAPlot");
	SigmaVSLgEVSLnAPlot->cd();

	TH3D* SigmaVSLgELnAPlotBkg= new TH3D("SigmaVSLgELnAPlotBkg","",100,-2,2,100,-1,5,100,10,90);
	SigmaVSLgELnAPlotBkg->GetZaxis()->SetTitle("#sigma_{X_{max}}");
	SigmaVSLgELnAPlotBkg->GetZaxis()->SetTitleSize(0.07);
	SigmaVSLgELnAPlotBkg->GetXaxis()->SetTitle("log_{10}(E/E_{0})");
	SigmaVSLgELnAPlotBkg->GetXaxis()->SetTitleSize(0.07);
	SigmaVSLgELnAPlotBkg->GetYaxis()->SetTitle("ln(A)");
	SigmaVSLgELnAPlotBkg->GetYaxis()->SetTitleSize(0.07);
	SigmaVSLgELnAPlotBkg->Draw();	

	TF2* SigmaVSLgELnAFitFcn= new TF2("SigmaVSLgELnAFitFcn",ParFitFcn,-3,3,-1,5,6);
	SigmaVSLgELnAFitFcn->SetParameters(SigmaPar0VSLnAFitFcn->GetParameter(0),SigmaPar0VSLnAFitFcn->GetParameter(1),SigmaPar0VSLnAFitFcn->GetParameter(2),SigmaPar1VSLnAFitFcn->GetParameter(0),SigmaPar1VSLnAFitFcn->GetParameter(1),SigmaPar1VSLnAFitFcn->GetParameter(2));
	SigmaVSLgELnAFitFcn->SetNpx(100);
	SigmaVSLgELnAFitFcn->SetNpy(100);
	SigmaVSLgELnAFitFcn->SetParNames("a0","a1","a2","b0","b1","b2");
	
	SigmaVSLgEVSLnAGraph->Fit("SigmaVSLgELnAFitFcn","R");
	SigmaVSLgELnAFitFcn->Draw("surf1 same");
 
	SigmaVSLgEVSLnAGraph->Draw("P0 same");

	cout<<"*** SIGMA FIT PAR ***"<<endl;
	cout<<"p0= {"<<SigmaVSLgELnAFitFcn->GetParameter(0)<<","<<SigmaVSLgELnAFitFcn->GetParameter(1)<<","<<SigmaVSLgELnAFitFcn->GetParameter(2)<<"}"<<endl;
	cout<<"p1= {"<<SigmaVSLgELnAFitFcn->GetParameter(3)<<","<<SigmaVSLgELnAFitFcn->GetParameter(4)<<","<<SigmaVSLgELnAFitFcn->GetParameter(5)<<"}"<<endl;


	TCanvas* LambdaVSLgEVSLnAPlot= new TCanvas("LambdaVSLgEVSLnAPlot","LambdaVSLgEVSLnAPlot");
	LambdaVSLgEVSLnAPlot->cd();

	TH3D* LambdaVSLgELnAPlotBkg= new TH3D("LambdaVSLgELnAPlotBkg","",100,-2,2,100,-1,5,100,400,1000);
	LambdaVSLgELnAPlotBkg->GetZaxis()->SetTitle("#lambda(X_{max})");
	LambdaVSLgELnAPlotBkg->GetZaxis()->SetTitleSize(0.07);
	LambdaVSLgELnAPlotBkg->GetXaxis()->SetTitle("log_{10}(E/E_{0})");
	LambdaVSLgELnAPlotBkg->GetXaxis()->SetTitleSize(0.07);
	LambdaVSLgELnAPlotBkg->GetYaxis()->SetTitle("ln(A)");
	LambdaVSLgELnAPlotBkg->GetYaxis()->SetTitleSize(0.07);
	LambdaVSLgELnAPlotBkg->Draw();	

	TF2* LambdaVSLgELnAFitFcn= new TF2("LambdaVSLgELnAFitFcn",ParFitFcn,-3,3,-1,5,6);
	LambdaVSLgELnAFitFcn->SetParameters(LambdaPar0VSLnAFitFcn->GetParameter(0),LambdaPar0VSLnAFitFcn->GetParameter(1),LambdaPar0VSLnAFitFcn->GetParameter(2),LambdaPar1VSLnAFitFcn->GetParameter(0),LambdaPar1VSLnAFitFcn->GetParameter(1),LambdaPar1VSLnAFitFcn->GetParameter(2));
	LambdaVSLgELnAFitFcn->SetNpx(100);
	LambdaVSLgELnAFitFcn->SetNpy(100);
	LambdaVSLgELnAFitFcn->SetParNames("a0","a1","a2","b0","b1","b2");
	
	LambdaVSLgEVSLnAGraph->Fit("LambdaVSLgELnAFitFcn","R");
	LambdaVSLgELnAFitFcn->Draw("surf1 same");
 
	LambdaVSLgEVSLnAGraph->Draw("P0 same");
	
	cout<<"*** LAMBDA FIT PAR ***"<<endl;
	cout<<"p0= {"<<LambdaVSLgELnAFitFcn->GetParameter(0)<<","<<LambdaVSLgELnAFitFcn->GetParameter(1)<<","<<LambdaVSLgELnAFitFcn->GetParameter(2)<<"}"<<endl;
	cout<<"p1= {"<<LambdaVSLgELnAFitFcn->GetParameter(3)<<","<<LambdaVSLgELnAFitFcn->GetParameter(4)<<","<<LambdaVSLgELnAFitFcn->GetParameter(5)<<"}"<<endl;

}//close macro


double ParFitFcn(double* x, double* par){

	double fval= 0.;
	double lgE= x[0];
	double lnA= x[1];

	double a0= par[0];  
	double a1= par[1];
	double a2= par[2];

	double b0= par[3];  
	double b1= par[4];
	double b2= par[5];

	double p0= a0 + a1*lnA + a2*lnA*lnA;  
	double p1= b0 + b1*lnA + b2*lnA*lnA;
		
	fval= p0 + p1*lgE;

	return fval;

}//close function


double ParFitFcn2(double* x, double* par){

	double fval= 0.;
	double lgE= x[0];
	double lnA= x[1];

	double a0= par[0];  
	double a1= par[1];
	double a2= par[2];

	double b0= par[3];  
	double b1= par[4];
	double b2= par[5];

	double c0= par[6];  
	double c1= par[7];
	double c2= par[8];

	double p0= a0 + a1*lnA + a2*lnA*lnA;  
	double p1= b0 + b1*lnA + b2*lnA*lnA;
	double p2= c0 + c1*lnA + c2*lnA*lnA;
		
	fval= p0 + p1*lgE + p2*lgE*lgE;

	return fval;

}//close function


